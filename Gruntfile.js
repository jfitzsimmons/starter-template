'use strict';
module.exports = function(grunt) {
 
  // load all grunt tasks
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
 
  grunt.initConfig({
 
      // watch for changes and trigger compass, jshint, uglify and livereload
      watch: {
        styles: {
          files: ['src/less/*.less'],
          tasks: ['less']
        },
        js: {
          files: ['src/js/*.js'],
          tasks: ['jshint', 'uglify']
        },
        livereload: {
          files: ['assets/**'],
          options: {
            livereload: true
          }
        }
      },
 
      // less compiling
      less: {
        development: {
          options: {
            compress: true,
            cleancss: true,
            sourceMap: true,
            sourceMapFilename: 'assets/css/main.css.map',
            sourceMapRootpath: '/assets/css' 
          },
          files: {
            "assets/css/main.css": "src/less/main.less"
          }
        }
      },
 
      // uglify to concat, minify, and make source maps
      uglify: {
        options: {
          beautify: false,
          mangle: false
        },
        scripts: {
          files: {
            'assets/js/script.min.js': [
              'src/js/*.js'
            ]
          }
        }
      }
 
      // image optimization
      // imagemin: {
      //   dist: {
      //     options: {
      //       optimizationLevel: 7,
      //       progressive: true
      //     },
      //     files: [{
      //       expand: true,
      //       cwd: 'assets/images/',
      //       src: '**/*',
      //       dest: 'assets/images/'
      //     }]
      //   }
      // },
 
      // deploy via rsync
      // deploy: {
      //     staging: {
      //         src: "./",
      //         dest: "~/path/to/theme",
      //         host: "user@host.com",
      //         recursive: true,
      //         syncDest: true,
      //         exclude: ['.git*', 'node_modules', '.sass-cache', 'Gruntfile.js', 'package.json', '.DS_Store', 'README.md', 'config.rb', '.jshintrc']
      //     },
      //     production: {
      //         src: "./",
      //         dest: "~/path/to/theme",
      //         host: "user@host.com",
      //         recursive: true,
      //         syncDest: true,
      //         exclude: '<%= rsync.staging.exclude %>'
      //     }
      // }
 
  });
 
  // rename tasks
//  grunt.renameTask('rsync', 'deploy');
 
  // register task
  grunt.registerTask('build', ['uglify', 'less']);
  grunt.registerTask('default', ['build', 'watch']);
 
};